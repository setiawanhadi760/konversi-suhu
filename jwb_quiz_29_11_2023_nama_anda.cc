/*
   Nama         : Setiawan Hadi Nugraha
   Kelas        : IF 1A
   NIM          : 301230007
   Tgl          : 29 November 2023
   
*/


#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int i = 1;
    float Data = 0.0, Rata = 0.0, Total = 0.0, Hasil = 0.0;

    
    do {
        cout << "Masukan data ke " << i << " : ";
        cin >> Data;
        Total += Data;
        i++;
      } while ( Data != 0);
    
    Hasil = i - 2;
    cout << "Banyaknya Data : " << Hasil << endl;
    cout << "Total Nilai Data : " <<  setprecision(2) << Total << endl;
    Rata = Total / Hasil;
    cout << "Rata-rata nilai Data : " << setprecision(2) << Rata << endl;
  return 0;
}


