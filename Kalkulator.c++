#include <iostream>
using namespace std;
int main()
{
    int a, b, pertambahan, pengurangan, perkalian, modulus, pembagian;
    cout << "Masukan Nilai a : ";
    cin >> a;
    cout << "Masukan Nilai b: ";
    cin >> b;
    pertambahan = a + b;
    pengurangan = a - b;
    perkalian = a * b;
    pembagian = a / b;
    modulus = a % b;
    cout << "---------------------------" << endl;
    cout << "| operasi | hasil operasi |" << endl;
    cout << "---------------------------" << endl;
    cout << "|  " << a << " + " << b << "  |\t " << pertambahan << "\t  |" << endl;
    cout << "|  " << a << " - " << b << "  |\t " << pengurangan << "\t  |" << endl;
    cout << "|  " << a << " x " << b << "  |\t " << perkalian << "\t  |" << endl;
    cout << "|  " << a << " : " << b << "  |\t " << pembagian << "\t  |" << endl;
    cout << "|  " << a << " % " << b << "  |\t " << modulus << "\t  |" << endl;
    cout << "---------------------------";
}
